package com.hexaware.package3;
public class BankAccount implements EmployeeDAO{
	private String name;
    private double accountbal;
    private static long counter=1000;
    private long acc_Id;
    
    BankAccount(long acc_Id, String name, double accountbal){
        this.name=name;
        this.accountbal=accountbal;
        this.acc_Id=counter++;

    }
    @Override
	public String toString() {
		return "BankAccount [name=" + name + ", accountbal=" + accountbal + ", acc_Id=" + acc_Id + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (acc_Id ^ (acc_Id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (acc_Id != other.acc_Id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	BankAccount(String name){
        this.name=name;
        this.acc_Id=counter++;
    }

	@Override
	public void deposit(int amt) {
		accountbal=accountbal+amt;		
	}

	@Override
	public void deposit(int amt, String S) {
		accountbal=accountbal+amt;
        System.out.println(S);
	}

	@Override
	public double withdraw(int amt) {
		if(amt<accountbal)
            accountbal=accountbal-amt;
        else
            System.out.println("Insufficient Balance");
    return amt; 
	}

	@Override
	public double withdraw(int amt, int S) {
		if(amt<accountbal){
            accountbal=accountbal-amt;}
        else
            System.out.println("Insufficient Balance");

        System.out.println(S);
    return amt; 
	}

	@Override
	public double showbal() {
		return this.accountbal;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public long getAccId() {
		return this.acc_Id;
	}
	

}
