package com.hexaware.package3;

import java.util.List;
import java.util.ArrayList;

public class RepositoryImplementation {
	List<BankAccount> list= new ArrayList<>();
    static private int index=0;
    
    List<BankAccount> addAccount(BankAccount obj){
        if(index>9) System.out.println("Array filled");
        list.add(obj);
        index++;
        return list;
    }
    
    public List<BankAccount> deleteAccount(List<BankAccount> list, int id) {
        int c=0;
        for( int i=0;i<list.size();i++){
        	if(list.get(i).getAccId()==id) {
        		System.out.println("Account with id = "+list.get(i).getAccId()+" deleted");
        		list.remove(i);
        	}
        }
        return list;
    }
    
    public void listAccounts(List<BankAccount> list){
        //create a new array with the size equal to the index;
        //copy the contents form the original array to this array.
        //return this new array.
        for(BankAccount account : list) {
        	System.out.println(account.getAccId()+"\t"+ account.getName()+ "\t" + account.showbal());
        }
    }

}
